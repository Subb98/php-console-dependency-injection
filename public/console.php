<?php declare(strict_types=1);

require_once __DIR__ . '/../vendor/autoload.php';

use App\Command\Handler\TestCommandHandler;
use App\Command\TestCommand;
use League\Container\Container;
use League\Container\ContainerAwareInterface;
use Symfony\Component\Console\Application;

$container = new Container();

// add test command handler to container
$container
    ->add('test_command_handler', TestCommandHandler::class)
    ->addArguments(['Test console application', __DIR__ . '/..'])
;

// add test command to container
$container
    ->add('test_command', TestCommand::class)
    ->addArgument($container->get('test_command_handler'))
;

// add test command to console application
$container
    ->add('application', Application::class)
    ->addMethodCall('add', [$container->get('test_command')])
;

// get and run console application
$container
    ->get('application')
    ->run()
;
