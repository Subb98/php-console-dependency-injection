<?php

namespace App\Command\Interface;

interface TestCommandHandlerInterface
{
    public function run(): void;
}
