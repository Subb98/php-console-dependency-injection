<?php

namespace App\Command;

use App\Command\Interface\TestCommandHandlerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestCommand extends Command
{
    private TestCommandHandlerInterface $handler;

    public function __construct(TestCommandHandlerInterface $handler)
    {
        $this->handler = $handler;

        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    protected static $defaultName = 'app:test';

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setDescription('Test command for check dependency injection work');
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->handler->run();

        return self::SUCCESS;
    }
}
