<?php

namespace App\Command\Handler;

use App\Command\Interface\TestCommandHandlerInterface;

class TestCommandHandler implements TestCommandHandlerInterface
{
    private string $applicationName;
    private string $rootDir;

    public function __construct(string $applicationName, string $rootDir)
    {
        $this->applicationName = $applicationName;
        $this->rootDir = $rootDir;
    }

    public function run(): void
    {
        echo sprintf("application name: '%s', root directory: '%s'\n", $this->applicationName, $this->rootDir);
    }
}
